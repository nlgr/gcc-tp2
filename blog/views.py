from django.shortcuts import render


posts = [
	{
		'author':'Lucas Guerrero',
		'title':'Continuous Integration',
		'content':'content1',
		'date':'May 16, 2019'
	},
	{
		'author':'Lucas Guerrero',
		'title':'Work Project',
		'content':'content2',
		'date':'May 16, 2019'
	}
]
def home(request):
	context = {
		'posts':posts,
		'title':'Continous Integration'
	}
	return render(request, 'blog/home.html', context)

def about(request):
	return render(request, 'blog/about.html', {'title':'About Page'})